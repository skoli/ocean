from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'moon.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    #url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
    #url(r'^accounts/', include('allauth.urls')),
    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tmms/', include('tmms.urls')),
)
