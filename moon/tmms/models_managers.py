from django.db import models

class TestcaseManager(models.Manager):
    def get_by_natural_key(self, ts_id):
        return self.get(ts_id=ts_id)
    
class ImpProgressManager(models.Manager):
    def get_by_natural_key(self, state):
        self.get(state=state)

class BandManager(models.Manager):
    def get_by_natural_key(self, band):
        return self.get(band=band)

class MeasurementManager(models.Manager):
    def get_by_natural_key(self, measurement):
        return self.get(measurement=measurement)
    
class TempVoltManager(models.Manager):
    def get_by_natural_key(self, temp, volt):
        return self.get(temp=temp, volt=volt)

class CustomerManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)

class ProductManager(models.Manager):
    def get_by_natural_key(self, product):
        return self.get(product=product)

class ReleaseManager(models.Manager):
    def get_by_natural_key(self, sw_rel):
        return self.get(sw_rel=sw_rel)

class TestCampaignManager(models.Manager):
    def get_by_natural_key(self, title):
        return self.get(title=title)

class VerdictManager(models.Manager):
    def get_by_natural_key(self, verdict):
        return self.get(verdict=verdict)

class TestParameterManager(models.Manager):
    def get_by_natural_key(self, rat, id):
        return self.get(rat=rat, id=id)
    
class TestResultsManager(models.Manager):
    def get_by_natural_key(self, testcase):
        return self.get(testcase=testcase)

class TestParamVamosManager(models.Manager):
    def get_by_natural_key(self, vamos_level, active_subchannel, scpir, vamos_profile):
        return self.get(vamos_level=vamos_level, 
                        active_subchannel=active_subchannel,
                        scpir=scpir, vamos_profile=vamos_profile)
        
class TestParam2GManager(models.Manager):
    def get_by_natural_key(self, alias, id):
        return self.get(alias=alias, id=id)
        
class TestParam3GManager(models.Manager):
    def get_by_natural_key(self, alias, id):
        return self.get(alias=alias, id=id)
    
class InterferenceParam4GManager(models.Manager):
    def get_by_natural_key(self, alias, id):
        return self.get(alias=alias, id=id)
    
class TestParam4GManager(models.Manager): 
    def get_by_natural_key(self, alias, id):
        return self.get(alias=alias, id=id)
        
        
    