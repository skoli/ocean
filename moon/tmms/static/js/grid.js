var grid;
var dataView;
var columnFilters = {};

function filter(item) {
    for (var columnId in columnFilters) {
        if (columnId !== undefined && columnFilters[columnId] !== "") {
            var c = grid.getColumns()[grid.getColumnIndex(columnId)];
            //if (item[c.field] != columnFilters[columnId]) { // for exact match
            if (item[c.field].toLowerCase().indexOf(columnFilters[columnId]) == -1) {
                return false;
            }
        }
    }
    return true;
}

var sortcol = "title";
var sortdir = 1;

function comparer(a, b) {
    var x = a[sortcol], y = b[sortcol];
    return (x === y ? 0 : (x > y ? 1 : -1));
}

var grouplist = [];
var grouping = {};

function setGroupCollapse(val){
    var len= grouplist.length
    if(len){
        grouplist[len-1]['collapsed'] = val;
    }
} 

function removeGrouping(args){
    for (i=0; i<grouplist.length; i++){
        if (grouplist[i]['getter'] ==  args.column.field) {
            grouplist.splice(i, 1);
            break;
        }
    }
    setGroupCollapse(true) 
}

function addGrouping(args){
    setGroupCollapse(false)
    grouping = {
        getter: args.column.field,
        formatter: function (g) {
            return  args.column.name + ":  " + g.value + "  <span style='color:green'>(" + g.count + " items)</span>";
        },
        collapsed:true
    };

    grouplist.push(grouping);
    dataView.setGrouping(grouplist);
}

function toggleFilterRow() {
  grid.setHeaderRowVisibility(!grid.getOptions().showHeaderRow);
}

function girdCommonInit(placeholder, data, columns, options){
    /*var checkboxSelector = new Slick.CheckboxSelectColumn({
        cssClass: "slick-cell-checkboxsel"
    });

    columns.unshift(checkboxSelector.getColumnDefinition());
    */
    var groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider({
        toggleExpandedCssClass: "fa fa-minus-square-o",
        toggleCollapsedCssClass: "fa fa-plus-square-o",
    });

    dataView = new Slick.Data.DataView({
        groupItemMetadataProvider: groupItemMetadataProvider,
        inlineFilters: true
    });

    grid = new Slick.Grid(placeholder, dataView, columns, options);

    // register the group item metadata provider to add expand/collapse group handlers
    grid.registerPlugin(groupItemMetadataProvider);
    grid.setSelectionModel(new Slick.CellSelectionModel());
    grid.setSelectionModel(new Slick.RowSelectionModel({selectActiveRow: false}));
    //grid.registerPlugin(checkboxSelector);

    // wire up model events to drive the grid
    dataView.onRowCountChanged.subscribe(function (e, args) {
        grid.updateRowCount();
        grid.invalidateRow(args.previous-1); // sanju work around
        grid.render();
    });
    
    dataView.onRowsChanged.subscribe(function (e, args) {
        args.rows.push(args.rows[args.rows.length-1]+1) // sanju work around
        grid.invalidateRows(args.rows);   
        grid.render();
    });

    $(grid.getHeaderRow()).delegate(":input", "change keyup", function (e) {
    var columnId = $(this).data("columnId");
    if (columnId != null) {
        // clear on Esc
        if (e.which == 27) {
            this.value = "";
        }
        columnFilters[columnId] =  $.trim($(this).val()).toLowerCase();
        dataView.refresh();
    }
    });

    grid.onHeaderRowCellRendered.subscribe(function(e, args) {
    $(args.node).empty();
    $("<input type='text'>")
        .data("columnId", args.column.id)
        .val(columnFilters[args.column.id])
        .appendTo(args.node);
    });

    return grid;
}

function initGrid(placeholder, data, columns){

    var options = {
    enableCellNavigation: true,
    editable: true,
    enableAddRow: false,
    showHeaderRow: true,
    headerRowHeight: 30,
    explicitInitialization: true,
    forceFitColumns: true
    };

	/*var checkboxSelector = new Slick.CheckboxSelectColumn({
      cssClass: "slick-cell-checkboxsel"
    });

    columns.unshift(checkboxSelector.getColumnDefinition());
    */
    
    grid = girdCommonInit(placeholder, data, columns, options);
    
    grid.onContextMenu.subscribe( function (e) {
        e.preventDefault();
        var cell = grid.getCellFromEvent(e);
        contextMenu(e, {
            menuSelector: "#context-menu-grid",
            menuSelected: function (invokedOn, selectedMenu) {
                /*var msg = "You selected the menu item '" + selectedMenu.text() +
                "' on the value '" + invokedOn.text() + "'";
                alert(msg);*/
                if (selectedMenu.text() === 'Toggle Quick Filter'){
                    toggleFilterRow();
                }
            }
        });
        
    });

    var headerMenuPlugin = new Slick.Plugins.HeaderMenu({});
  
    headerMenuPlugin.onCommand.subscribe(function (e, args) {
        var menu = args.column.header.menu;
        if (args.command == 'group') {
            addGrouping(args);
            menu.items.pop();
            menu.items.push({
                title: "Remove Group",
                command: "ungroup"
            });
        }
        else if (args.command == 'ungroup'){
            removeGrouping(args)
            menu.items.pop()
            menu.items.push({
                title: "Group By Column",
                command: "group"
            });
        }
        
        dataView.refresh();
    });
  
    grid.registerPlugin(headerMenuPlugin);
  
    var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"));
    var columnpicker = new Slick.Controls.ColumnPicker(columns, grid, options);

    
    grid.onSort.subscribe(function (e, args) {
        sortdir = args.sortAsc ? 1 : -1;
        sortcol = args.sortCol.field;
        // using native sort with comparer
        // preferred method but can be very slow in IE with huge datasets
        dataView.sort(comparer, args.sortAsc);
    });
    
    var i = 0;
    for (i = 0; i < columns.length; i++){
    columns[i].header = {
        menu: {
            items: [ {
                iconImage: "/static/slickgrid/images/sort-asc.gif",
                title: "Sort Ascending",
                command: "sort-asc"
            },
            {
                iconImage: "/static/slickgrid/images/sort-desc.gif",
                title: "Sort Descending",
                command: "sort-desc"
            },
            {
                title: "Group By Column",
                command: "group",
                tooltip: "group by this column"
            }]
        }
    };
    }

    grid.init();
    toggleFilterRow();
    // initialize the model after all the events have been hooked up
    dataView.beginUpdate();
    dataView.setItems(data);
    dataView.setFilter(filter);
    dataView.endUpdate();
    dataView.refresh();

}

function initFieldsGrid(placeholder, data, columns){
    var options = {enableCellNavigation: true,
        showHeaderRow: true,
        headerRowHeight: 30,
        explicitInitialization: true,
        forceFitColumns: true
    }
    
    grid =  girdCommonInit(placeholder, data, columns, options);

    grid.init();
    dataView.setGrouping({
        getter: "table",
        formatter: function (g) {
            return g.value;
        }
    });

    grid.onDragInit.subscribe(function (e, dd) {
        // prevent the grid from cancelling drag'n'drop by default
        e.stopImmediatePropagation();
    });

    grid.onDragStart.subscribe(function (e, dd) {
        var cell = grid.getCellFromEvent(e);
        if (!cell) {
        return;
        }

        dd.row = cell.row;
        if (!data[dd.row]) {
            return;
        }

        if (Slick.GlobalEditorLock.isActive()) {
            return;
        }

        e.stopImmediatePropagation();
        dd.mode = "filter-zone";

        var selectedRows = grid.getSelectedRows();

        if (!selectedRows.length || $.inArray(dd.row, selectedRows) == -1) {
            selectedRows = [dd.row];
            grid.setSelectedRows(selectedRows);
        }

        dd.rows = selectedRows;
        dd.count = selectedRows.length;

        var proxy = $("<span></span>")
            .css({
              position: "absolute",
              display: "inline-block",
              padding: "4px 10px",
              background: "#e0e0e0",
              border: "1px solid gray",
              "z-index": 99999,
              "-moz-border-radius": "8px",
              "-moz-box-shadow": "2px 2px 6px silver"
            })
            .text("Drag to Filter area to add filter" + dd.count + " selected row(s)")
            .appendTo("body");

        dd.helper = proxy;

        $(dd.available).css("background", "pink");

        return proxy;
    });

    grid.onDrag.subscribe(function (e, dd) {
        if (dd.mode != "filter-zone") {
            return;
        }
        dd.helper.css({top: e.pageY + 5, left: e.pageX + 5});
    });

    grid.onDragEnd.subscribe(function (e, dd) {
        if (dd.mode != "filter-zone") {
            return;
        }
        dd.helper.remove();
        $(dd.available).css("background", "beige");
    });

    $.drop({mode: "mouse"});
    $("#filter-dropzone")
        .bind("dropstart", function (e, dd) {
            if (dd.mode != "filter-zone") {
                return;
            }
            $(this).css("background", "yellow");
        })
        .bind("dropend", function (e, dd) {
            if (dd.mode != "filter-zone") {
                return;
            }
            $(dd.available).css("background", "pink");
        })
        .bind("drop", function (e, dd) {
            if (dd.mode != "filter-zone") {
                return;
            }
            var rowsToDelete = dd.rows.sort().reverse();
            for (var i = 0; i < rowsToDelete.length; i++) {
                data.splice(rowsToDelete[i], 1);
            }
            grid.invalidate();
            //grid.setSelectedRows([]);
        });


    // initialize the model after all the events have been hooked up
    dataView.beginUpdate();
    dataView.setItems(data);
    dataView.setFilter(filter);
    dataView.endUpdate();
    dataView.refresh();
}


