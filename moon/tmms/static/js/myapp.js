function contextMenu(e, settings){
    $(settings.menuSelector)
        .data("invokedOn", $(e.target))
        .show()
        .css({
            position: "absolute",
            left: getLeftLocation(e),
            top: getTopLocation(e)
        })
        .off('click')
        .on('click', function (e) {
            $(this).hide();
    
            var $invokedOn = $(this).data("invokedOn");
            var $selectedMenu = $(e.target);
            
            settings.menuSelected.call(this, $invokedOn, $selectedMenu);
            return false;
    });
    //make sure menu closes on any click
    $(document).click(function () {
        $(settings.menuSelector).hide();
    });
    

    function getLeftLocation(e) {
        var offsetX = $('#left-panel').width();
        var mouseWidth = e.pageX; // -(offsetX);
        var pageWidth = $(window).width();
        var menuWidth = $(window).width();
        
        // opening menu would pass the side of the page
        if (mouseWidth + menuWidth > pageWidth &&
            menuWidth < mouseWidth) {
            return mouseWidth - menuWidth;
        } 
        return mouseWidth;
    } 

    function getTopLocation(e) {
        var offsetY = $('#header').height();
        var mouseHeight = e.pageY;// - (offsetY);
        var pageHeight = $(window).height();
        var menuHeight = $(window).height();

        // opening menu would pass the bottom of the page
        if (mouseHeight + menuHeight > pageHeight &&
            menuHeight < mouseHeight) {
            return mouseHeight - menuHeight;
        } 
        return mouseHeight;
    }
}

function launchQueryEditor(){
	loadURL('queryeditor', $('#content'));
	return false
}

 $('nav li').on("contextmenu", function(e){
 	e.preventDefault();
 	contextMenu(e, {
            menuSelector: "#context-menu-side-li",
            menuSelected: function (invokedOn, selectedMenu) {
                /*var msg = "You selected the menu item '" + selectedMenu.text() +
                "' on the value '" + invokedOn.text() + "'";
                alert(msg);*/
                if (selectedMenu.text() === 'Edit'){
                	
                    launchQueryEditor();
                }
                else if (selectedMenu.text() === 'Create Query'){
                	launchQueryEditor();
                }
                
 				
            }
        });
 });
