
from create_test_data import RAT_NS

file_2g = 'test_data_2g.json'
file_3g = 'test_data_3g.json'
file_dc = 'test_data_dc.json'
file_4g = 'test_data_4g.json'
file_tp = 'test_data_tp.json'

trans_scheme = ['RXDIV', 'TXDIV']

call_type_2g = ['CS', 'PS']    
test_mode_2g = ['TESTA', 'TESTB']
coding_scheme_2g = ['GMSK', '8PSK', 'AQPSK']
coding_scheme_egprs = ['MCS5', 'MCs6', 'MCS7', 'MCS8', 'MCS9']

VAMOS_LEVEL = ['Vamos1', 'Vamos2']
VAMOS_PROFILE = ['TWO_USER', 'TWO_USER_WITH_DTX']

coding_scheme_4g = ['QPSK', '16QAM', '64QAM']
fading_profile_2g = ['NoFading', 'GSM_6PATH_TU50', 'GSM_6PATH_TU3']
fading_profile_3g = ['NoFading', '3GPP_Case1', '3GPP_PA3', '3GPP_PB3', '3GPP_VA30']
fading_profile_dc = ['3GPP_DC_PA3', '3GPP_DC_PB3', '3GPP_DC_VA30']
fading_profile_4g = ['NoFading', '3GPP_LTE_EVA5', '3GPP_LTE_EVA70']
antenna_config = ['SISO', 'MIMO']
hsdpa_beta_pair = [(2, 15), (12, 15), (15, 8), (15, 4)]
hsupa_beta_pair = [(10, 15), (6, 15), (15, 9), (2, 15), (15, 0)]
call_type_3g = ['CS', 'PS']
test_mode_3g = ['RMC12']
coding_scheme_3g = ['RMC12']
call_type_4g = ['PS']
test_mode_4g = ['TM2', 'TM4']


dl_rb = [25, 100]
ul_rb = [100]
channel_type = ['PUCCH', 'PUSCH']
rb_pos = [0, 5]
power = ['Min', 'Max', 0, 23.0]
call_type_4g = ['RMC']
ns_values = ['NS01', 'NS03']
bandwidths = [1.4, 3, 5]
fading_setup = ['MIMO2x2']

def log(msg):
    print msg

def TestParam2G(f):
    i = 0
    for nw in range(1, len(RAT_NS['2G'])+1):
        for ts in trans_scheme[:1]:
            if nw == 1:
                call =  call_type_2g[0]
                tm = ""
            else:
                call =  call_type_2g[1]
                tm = test_mode_2g[0]
            for dl_cs in coding_scheme_2g[:2]:
                for ul_cs in coding_scheme_2g[:2]:
                    for fs in fading_setup:
                        for fp in fading_profile_2g:
                            for ant in antenna_config:
                                i+=1
                                f.write('{"model": "tmms.TestParam2G", ')
                                f.write('"pk": %d, ' %i)
                                f.write('"fields": {')
                                f.write('"nw_srv": %d, '%nw)
                                f.write('"trans_scheme": "%s", '%ts)
                                f.write('"call_type": "%s", '%call)
                                f.write('"test_mode": "%s", '%tm)
                                f.write('"dl_coding_scheme": "%s", '%dl_cs)
                                f.write('"ul_coding_scheme": "%s", '%ul_cs)
                                f.write('"fading_setup": "%s", '%fs)
                                f.write('"fading_profile": "%s", '%fp)
                                f.write('"antenna_config": "%s", '%ant)
                                f.write('"tch_power_level": "-102.0", ')
                                f.write('"tch_time_slot": 3, ')
                                f.write('"number_of_slots": 3, ')
                                f.write('"amr_channel_rate": "", ')
                                f.write('"pcl": "5", ')
                                f.write('"freq_hopping": false')
                                f.write('} },\n')
    log("total TestParam2G table records = %d"%i)
    return i

def TestParamVamos(f):
    i=0
    for vlevel in VAMOS_LEVEL:
        for tsc_no in [5]:
            for scpir in ['0.0', '-4.0', '-8.0']:
                for vp in VAMOS_PROFILE: 
                    i+=1
                    f.write('{"model": "tmms.TestParamVamos", ')
                    f.write('"pk": %d, ' %i)
                    f.write('"fields": {')
                    f.write('"vamos_level": "%s", '%vlevel)
                    f.write('"active_subchannel": 1, ')
                    f.write('"tsc_set_active_user": 1, ')
                    f.write('"tsc_number_active_user": %d, '%tsc_no)
                    f.write('"tsc_set_other_user": 2, ')
                    f.write('"tsc_number_other_user": %d, '%tsc_no)
                    f.write('"scpir": "%s", '%scpir)
                    f.write('"vamos_profile": "%s" '%vp)
                    f.write('} },\n')
    log("total TestParamVamos table records = %d"%i)
    return i


def TestParam3G(f):
    i=0
    start = len(RAT_NS['2G'])
    end = start + len(RAT_NS['3G'])
    for nw in range(start+1, end+1):
        for ts in trans_scheme[:1]:
            if nw == 4:
                call = call_type_3g[0]
            else:
                call = call_type_3g[1]
            for tm in test_mode_3g: 
                for dl_cs in coding_scheme_3g:
                    for ul_cs in coding_scheme_3g:
                        for fs in fading_setup:
                            for fp in fading_profile_3g:
                                for ant in antenna_config:
                                    i+=1
                                    f.write('{"model": "tmms.TestParam3G", ')
                                    f.write('"pk": %d, ' %i)
                                    f.write('"fields": {')
                                    f.write('"nw_srv": %d, '%nw)
                                    f.write('"trans_scheme": "%s", '%ts)
                                    f.write('"call_type": "%s", '%call)
                                    f.write('"test_mode": "%s", '%tm)
                                    f.write('"dl_coding_scheme": "%s", '%dl_cs)
                                    f.write('"ul_coding_scheme": "%s", '%ul_cs)
                                    f.write('"fading_setup": "%s", '%fs)
                                    f.write('"fading_profile": "%s", '%fp)
                                    f.write('"antenna_config": "%s", '%ant)
                                    f.write('"cpich_level": "-102.0", ')
                                    f.write('"dpdch_level": "-54.0", ')
                                    f.write('"Ior_Ioc": "-3", ')
                                    f.write('"Ior": "-70.0", ')
                                    f.write('"ue_output_power": "23.0", ')
                                    f.write('"beta_c": 1, ')
                                    f.write('"beta_d": 1 ')
                                    f.write('} },\n')
    log("total TestParam3G table records = %d"%i)
    return i

    
def TestParam4G(f):
    i = 0
    start = len(RAT_NS['2G']) + len(RAT_NS['3G'])
    end = start + len(RAT_NS['4G'])
    for nw in range(start+1, end+1):
        for ts in trans_scheme[:1]:
            call = call_type_4g[0]
            for tm in test_mode_4g: 
                for dl_cs in coding_scheme_4g:
                    for ul_cs in coding_scheme_4g[:1]:
                        for fs in fading_setup:
                            for fp in fading_profile_4g[1:2]:
                                for ant in antenna_config:
                                    for bw in bandwidths:
                                        for nw_val in ns_values: 
                                            for rb_dl in dl_rb:
                                                for rb_ul in ul_rb:
                                                    for ch_type in channel_type:
                                                        i+=1
                                                        f.write('{"model": "tmms.TestParam4G", ')
                                                        f.write('"pk": %d, ' %i)
                                                        f.write('"fields": {')
                                                        f.write('"nw_srv": %d, '%nw)
                                                        f.write('"trans_scheme": "%s", '%ts)
                                                        f.write('"call_type": "%s", '%call)
                                                        f.write('"test_mode": "%s", '%tm)
                                                        f.write('"dl_coding_scheme": "%s", '%dl_cs)
                                                        f.write('"ul_coding_scheme": "%s", '%ul_cs)
                                                        f.write('"fading_setup": "%s", '%fs)
                                                        f.write('"fading_profile": "%s", '%fp)
                                                        f.write('"antenna_config": "%s", '%ant)
                                                        f.write('"bandwidth": "%s", '%bw)
                                                        f.write('"cell_power": "-102.0", ')
                                                        f.write('"ns_value": "%s", '%nw_val)
                                                        f.write('"dl_rb": %d, '%rb_dl)
                                                        f.write('"ul_rb": %d, '%rb_ul)
                                                        f.write('"channel_type": "%s", '%ch_type)
                                                        f.write('"rb_pos": 0, ')
                                                        f.write('"ue_ouput_power": "23.0", ')
                                                        f.write('"snr": "3.0" ')
                                                        f.write('} },\n')
    log("total TestParam4G table records = %d"%i)
    return i
"""
def TestParameter(f, rec_2g, rec_3g, rec_4g):
    i = 0
    for param in range(1, rec_2g+1):
        i+=1
        f.write('{"model": "tmms.TestParameter", ')
        f.write('"pk": %d, ' %i)
        f.write('"fields": {')
        f.write('"rat": 1, ')
        f.write('"test_param_2g": %d '%param)
        f.write('} },\n')
    for param in range(1, rec_3g+1):
        i+=1
        f.write('{"model": "tmms.TestParameter", ')
        f.write('"pk": %d, ' %i)
        f.write('"fields": {')
        f.write('"rat": 2, ')
        f.write('"test_param_3g": %d '%param)
        f.write('} },\n')
    for param in range(1, rec_4g+1):
        i+=1
        f.write('{"model": "tmms.TestParameter", ')
        f.write('"pk": %d, ' %i)
        f.write('"fields": {')
        f.write('"rat": 3, ')
        f.write('"test_param_4g": %d '%param)
        f.write('} },\n')
    
    log("total TestParameter table records = %d"%i)
    return i 
"""

def main():
    f_2g = open(file_2g, 'w')
    f_3g = open(file_3g, 'w')
    f_4g = open(file_4g, 'w')
    #f_tp = open(file_tp, 'w')
    
    f_2g.write('[\n')
    f_3g.write('[\n')
    f_4g.write('[\n')
    #f_tp.write('[\n')
    
    TestParam2G(f_2g)
    TestParamVamos(f_2g)
    TestParam3G(f_3g)
    TestParam4G(f_4g)
    #TestParameter(f_tp, rec_2g, rec_3g, rec_4g)
    
    f_2g.write(']')
    f_3g.write(']')
    f_4g.write(']')
    #f_tp.write(']')
    
    f_2g.close()
    f_3g.close()
    f_4g.close()
    #f_tp.close()
    
if "__main__" == __name__:
    main()
     
    