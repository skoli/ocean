from random import randint, randrange
from decimal import Decimal
import datetime

file_main = 'test_data.json'
file_tc = 'test_data_tc.json'
file_tr = 'test_data_tr.json'
file_cam = 'test_data_cam.json'

CUSTOMERS = [('ABC', 'San jose, CA'),
             ('XYZ', 'Bangalore, India'),
             ('PQR', 'Munich, Germany')
             ]
PRODUCTS = ['Venus', 'Mars', 'Pluto', 'Santa', 'KitKat', 'CandyBar', 'Lotus', 'Foryo', 'Galaxy']

VERDICTS = ['PASS', 'FAIL', 'INCONC', 'BLOCKED', 'N/A']

HW_VER = ['V1', 'V2']
SW_VER = ['CW01', 'CW02', 'CW03', 'CW04', 'CW05']

TEST_CAM_2G = ["Regression_2g", "Weekly_2g", "Priority_1_2g"]
TEST_CAM_3G = ["Regression_3g", "Weekly_3g", "Priority_1_3g"]
TEST_CAM_4G = ["Regression_4g", "Weekly_4g", "Priority_1_4g"]

TEMP_VOLT = [('NT', 'NV'), ('HT', 'HV'), ('HT', 'LV'), ('LT', 'LV'), ('LT', 'HV')]

RAT_NS = {'2G' : ['GSM', 'GPRS', 'EGPRS'], 
          '3G' : ['WCDMA', 'HSDPA', 'HSUPA', 'DC-HSDPA', 'DC-HSUPA', 'TD-SCDMA', 'TD-HSDPA', 'TD-HSUPA'],
          '4G' : ['LTE', 'LTE-TDD', 'LTE-FDD', 'LTE-FDD-CA', 'LTE-TDD-CA']
          }

FEATURES = ['WIC', 'TIC', 'DC', 'ET', 'CA', 'VAMOS']
STATES = ["draft", "in_review", "under_development", "verified", "validated", "completed"]
TAGS = ["GSM", "WIC", "Interference", "BadScenario", "Extreme", "CA", "CustomerPrio"]

BAND_2G = ['GSM850', 'GSM900', 'GSM1800', 'GSM1900']
BAND_3G = ['FDD1', 'FDD2', 'FDD4', 'FDD5', 'FDD8']
BAND_4G = [1, 2, 3, 4, 5, 7, 20]       
MEAS_TYPE = ['MaxPower', 'EVM', 'PeackCodeDomainError', 'FreqError', 'RefSens', 'BER', 'BLER', 'FER']
CHANNEL = [32, 10560, 716]


def log(msg):
    print msg
    
def generate_dates(start_date, end_date):
    date_list = []
    td = datetime.timedelta(hours=24)
    current_date = start_date
    while current_date <= end_date:
        date_list.append(current_date)
        current_date += td
    return date_list

def generate_tc_id():
    a = randint(4, 20)
    b = randint(1, 10)
    c = randint(1, 30)
    return "%s.%s.%s"%(a, b, c)
            
         
def Rat(f):
    i = 0
    for rat in ['2G', '3G', '4G']:
        i+=1
        f.write('{"model":"tmms.Rat", "pk":%d, "fields":{"rat":"%s" } },\n '%(i, rat))
    log("total Rat table records = %d"%i)
    return i
           
def NwSrv(f):
    i = 0
    for ns in RAT_NS['2G']:
        i+=1
        f.write('{"model":"tmms.NwSrv", "pk":%d, "fields":{"rat":1, "network":"%s" } },\n '%(i, ns))
    for ns in RAT_NS['3G']:
        i+=1
        f.write('{"model":"tmms.NwSrv", "pk":%d, "fields":{"rat":2, "network":"%s" } },\n '%(i, ns))
    for ns in RAT_NS['4G']:
        i+=1
        f.write('{"model":"tmms.NwSrv", "pk":%d, "fields":{"rat":3, "network":"%s" } },\n '%(i, ns))
    log("total NwSrv table records = %d"%i)
    return i

def Band(f):
    i = 0
    for b in BAND_2G:
        i+=1
        f.write('{"model":"tmms.Band", "pk":%d, "fields":{"rat":1, "band":"%s"} },\n '%(i, b))
    for b in BAND_3G:
        i+=1
        f.write('{"model":"tmms.Band", "pk":%d, "fields":{"rat":2, "band":"%s"} },\n '%(i, b))
    for b in BAND_4G:
        i+=1
        f.write('{"model":"tmms.Band", "pk":%d, "fields":{"rat":3, "band":"%s"} },\n '%(i, b))
    log("total Band table records = %d"%i)
    return i

def Feature(f):
    i = 0
    for feature in FEATURES:
        i+=1
        f.write('{"model":"tmms.Feature", "pk":%d, "fields":{"feature":"%s" } },\n '%(i, feature))
    log("total Feature table records = %d"%i)
    return i

def ImpStatus(f):
    i = 0
    for status in STATES:
        i+=1 
        f.write('{"model":"tmms.ImpStatus", "pk":%d, "fields":{"status":"%s" } },\n '%(i, status))
    log("total ImpStatus table records = %d"%i)
    return i

def Testcase(f, pk, nw_srv_start, nw_srv_end):
    i = pk
    for nw_id in range(nw_srv_start, nw_srv_end):
        for k in range(10):
            i+=1
            tc_id = generate_tc_id()
            tc_title = " Test case title goes here."
            feature = randint(1, len(FEATURES))
            reference = "dummy reference"
            description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod \
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo \
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse \
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non \
proident, sunt in culpa qui officia deserunt mollit anim id est laborum." 
            status = randint(1, len(STATES))
            f.write('{"model":"tmms.Testcase", "pk":%d, "fields":{"tc_id":"%s", \
"tc_title":"%s", "feature":%d, "nw_srv":%d, "reference":"%s", "description": "%s", \
"imp_status": %d} },\n '%(i, tc_id, tc_title, feature, nw_id, reference, description, status))
                    
    log("total Testcase table records = %d"%i)
    return i

def ImpProgress(f):
    i = 0
    start_date = datetime.datetime(2010, 1, 25, 8, 10, 0)
    end_date = datetime.datetime(2010, 1, 30, 8, 10, 0)
    for d in generate_dates(start_date, end_date):
        for j in range(1, len(STATES)+1):
            i +=1
            f.write('{"model":"tmms.ImpProgress", "pk":%d, "fields":{"progress_datestamp":"%s", \
"progress_timestamp":"%s", "imp_status":%d, "count":%d  } },\n '%(i, d.date(), d, j, randint(25, 100)))
    log("total ImpProgress table records = %d"%i)
    return i

def TempVolt(f):
    i = 0
    for tv in TEMP_VOLT:
        i+=1
        f.write('{"model":"tmms.TempVolt", "pk":%d, "fields":{"temp":"%s", "volt": "%s"} },\n '%(i, tv[0], tv[1]))
    log("total TempVolt table records = %d"%i)
    return i

def Measurement(f):
    i = 0
    for m in MEAS_TYPE:
        i+=1
        f.write('{"model":"tmms.Measurement", "pk":%d, "fields":{"measurement":"%s" } },\n '%(i, m))
    log("total Measurement table records = %d"%i)
    return i

def Tag(f):
    i = 0
    for t in TAGS:
        i+=1 
        f.write('{"model":"tmms.Tag", "pk":%d, "fields":{"tag":"%s" } },\n '%(i, t))
    log("total Tag table records = %d"%i)    
    return i

def TestcaseTagging(f, total_tc):
    for i in range(1, 21):
        tag = randint(1, len(TAGS))
        tc_id = randint(1, total_tc)                      
        f.write('{"model":"tmms.TestcaseTagging", "pk":%d, "fields":{"tag":%d, "testcase":%d } },\n '%(i, tag, tc_id))
    log("total TestcaseTagging table records = %d"%i)
    return i

def Verdict(f):
    i = 0
    for v in VERDICTS:
        i+=1
        f.write('{"model":"tmms.Verdict", "pk":%d, "fields":{"verdict":"%s" } },\n '%(i, v))
    log("total Verdict table records = %d"%i)
    return i

def Customer(f):
    i = 0
    for cust in CUSTOMERS:
        i+=1
        f.write('{"model":"tmms.Customer", "pk":%d, "fields":{"name":"%s", "details":"%s" } },\n '%(i, cust[0], cust[1]))
    return i 

def Product(f, customers):
    i = 0
    for product in PRODUCTS:
        k = randint(1, customers)
        i+=1
        f.write('{"model":"tmms.Product", "pk":%d, "fields":{"customer":%d, "product":"%s" } },\n '%(i, k, product))
    return i

def Release(f):
    i = 0
    j = 0
    for product in PRODUCTS:
        j+=1
        for hw in HW_VER:
            for sw in SW_VER:
                i+=1
                f.write('{"model":"tmms.Release", "pk":%d, "fields":{"product":%d, "hw_rel":"%s", "sw_rel":"%s"} },\n '%(i, j, hw, product+"_"+hw+"_"+sw))
    return i
 
def TestCampaignName(f):
    i = 0
    for test_cam in TEST_CAM_2G:
        i+=1
        f.write('{"model":"tmms.TestCampaignName", "pk":%d, "fields":{"title":"%s"} },\n '%(i, test_cam))
    for test_cam in TEST_CAM_3G:
        i+=1
        f.write('{"model":"tmms.TestCampaignName", "pk":%d, "fields":{"title":"%s"} },\n '%(i, test_cam))
    for test_cam in TEST_CAM_4G:
        i+=1
        f.write('{"model":"tmms.TestCampaignName", "pk":%d, "fields":{"title":"%s"} },\n '%(i, test_cam))
    log("total TestCampaignName table records = %d"%i)
    return i 
"""        
def TestCampaign(f, test_cams):
    i = 0
    for test_cam in range(1, test_cams+1):
        for testcase in range(1, 21):
            for channel in CHANNEL:
                i+=1 
                f.write('{"model":"tmms.TestCampaign", "pk":%d, "fields":{"title":%d, \
"temp_volt":%d, "rat":%d, "band":%d, "channel":%d, "testcase":%d, "test_param_2g":%d} },\n '
                        %(i, test_cam, 1, 1, 1, channel, testcase, 1))
    log("total TestCampaign table records = %d"%i)
    return i
"""
cam2g = len(TEST_CAM_2G)
cam3g = len(TEST_CAM_3G)
cam4g = len(TEST_CAM_4G)
def TestCampaign2G(f):
    i = 0
    for test_cam in range(1, cam2g+1):
        for j in range(20):
            testcase = randint(1, 30)
            i+=1 
            f.write('{"model":"tmms.TestCampaign2G", "pk":%d, "fields":{"title":%d, \
"temp_volt":1, "rat":1, "band":2, "channel":32, "testcase":%d, "test_param":10} },\n '
                    %(i, test_cam, testcase))
    log("total TestCampaign table records = %d"%i)

def TestCampaign3G(f):    
    i=0            
    for test_cam in range(cam2g+1, cam2g+cam3g+1):
        for j in range(20): 
            testcase = randint(31, 110)
            i+=1 
            f.write('{"model":"tmms.TestCampaign3G", "pk":%d, "fields":{"title":%d, \
"temp_volt":1, "rat":2, "band":5, "channel":10560, "testcase":%d, "test_param":50} },\n '
                    %(i, test_cam, testcase))
    log("total TestCampaign table records = %d"%i)

def TestCampaign4G(f):
    i=0
    for test_cam in range(cam2g+cam3g+1, cam2g+cam3g+cam4g+1):
        for j in range(20): 
            testcase = randint(111, 160)
            i+=1 
            f.write('{"model":"tmms.TestCampaign4G", "pk":%d, "fields":{"title":%d, \
"temp_volt":1, "rat":3, "band":12, "channel":716, "testcase":%d, "test_param":300} },\n '
                    %(i, test_cam, testcase))
            
    log("total TestCampaign table records = %d"%i)
    return i

    
def TestPlan(f):
    i = 0
    k = 1
    rel = []
    for j in range(1, 5):
        i+=1
        f.write('{"model":"tmms.TestPlan", "pk":%d, "fields":{"release":%d, "campaign2g":%d } },\n '%(i, j, k))
        rel.append(j)
    log("total TestPlan table records = %d"%i)
    return rel


"""
def TestResults(f, plan_rel, total_tc, total_tp):
    i=0
    ch = 32
    path="c:/abc.txt"
    test_paramete_id = range(1, total_tp, 50)
    for r in plan_rel:
        j = 0
        for testcase in range(1, total_tc, 10):
            i+=1
            d = Decimal(randrange(100000))/100
            ul = Decimal(randrange(100000))/100   
            ll = Decimal(randrange(100000))/100
            v = randint(1, len(VERDICTS))
            meas = randint(1, len(MEAS_TYPE))
            td = datetime.timedelta(minutes=i)
            t = datetime.datetime.now() + td
            f.write('{"model":"tmms.TestResult", "pk":%d, "fields":{"release":%d, \
"temp_volt":1, "band":1, "channel":%d, "testcase":%d, "test_parameter": %d, "measurement": %d, \
"number_of_frames": 10000, "data":"%.2f", "unit":"dBm", "upper_limit":"%.2f", \
"lower_limit":"%.2f", "verdict":%d, "exec_timestamp":"%s", "results_file_path":"%s" } },\n '
                    % (i, r, ch, testcase, test_paramete_id[j], meas, d, ul, ll, v, t, path ))
            j+=1
            
    log("total TestResult table records = %d"%i)
    return i
"""
def TestResults(f, rat, plan_rel, tc_id_start, tc_id_end, total_tp, tp_step_size):
    i=0
    ch = 32
    path="c:/abc.txt"
    test_paramete_id = range(1, total_tp, tp_step_size)
    for r in plan_rel:
        j = 0
        for testcase in range(tc_id_start, tc_id_end, 3):
            i+=1
            d = Decimal(randrange(100000))/100
            ul = Decimal(randrange(100000))/100   
            ll = Decimal(randrange(100000))/100
            v = randint(1, len(VERDICTS))
            meas = randint(1, len(MEAS_TYPE))
            td = datetime.timedelta(minutes=i)
            t = datetime.datetime.now() + td
            f.write('{"model":"tmms.TestResults%s", "pk":%d, "fields":{"release":%d, \
"temp_volt":1, "band":1, "channel":%d, "testcase":%d, "test_param": %d, "measurement": %d, \
"number_of_frames": 10000, "data":"%.2f", "unit":"dBm", "upper_limit":"%.2f", \
"lower_limit":"%.2f", "verdict":%d, "exec_timestamp":"%s", "results_file_path":"%s" } },\n '
                    % (rat, i, r, ch, testcase, test_paramete_id[j], meas, d, ul, ll, v, t, path ))
            j+=1
            
    log("total TestResult%s table records = %d"%(rat, i))
    return i


            
def main():
    
    f_main = open(file_main, 'w')
    f_tc = open(file_tc, 'w')
    f_tr = open(file_tr, 'w')
    f_cam = open(file_cam, 'w')
    
    f_main.write('[\n')
    f_tc.write('[\n')
    f_tr.write('[\n')
    f_cam.write('[\n')
    
    ##1
    Rat(f_main)
    NwSrv(f_main)
    Band(f_main)
    TempVolt(f_main)
    Feature(f_main)
    ImpStatus(f_main)
    ImpProgress(f_main)
    Measurement(f_main)
    customers = Customer(f_main)
    Product(f_main, customers)
    Verdict(f_main)
    Release(f_main)
    TestCampaignName(f_cam)
    TestCampaign2G(f_cam)
    TestCampaign3G(f_cam)
    TestCampaign4G(f_cam)
    plan_rel = TestPlan(f_cam)
    
    ##2
    total_tc_2g = Testcase(f_tc, 0, 1, len(RAT_NS['2G'])+1)
    total_tc_3g = Testcase(f_tc, total_tc_2g, len(RAT_NS['2G'])+1, len(RAT_NS['2G'])+len(RAT_NS['3G'])+1)
    total_tc_4g = Testcase(f_tc, total_tc_3g, len(RAT_NS['2G'])+len(RAT_NS['3G'])+1, 
                           len(RAT_NS['2G'])+len(RAT_NS['3G'])+len(RAT_NS['4G'])+1)
    
    
    Tag(f_tc)
    TestcaseTagging(f_tc, total_tc_4g)
    
    ##3
    #TestResults(f_tr, plan_rel, total_tc, 1592)
    TestResults(f_tr, '2G', plan_rel, 1, 30, 72, 5)
    TestResults(f_tr, '3G', plan_rel, 31, 110, 80, 3)
    TestResults(f_tr, '4G', plan_rel, 111, 160, 1440, 50)
    
    f_main.write(']')
    f_tc.write(']')
    f_tr.write(']')
    f_cam.write(']')

    f_main.close()
    f_tc.close()
    f_tr.close()
    f_cam.close()

if "__main__" == __name__:
    main()
     
    
