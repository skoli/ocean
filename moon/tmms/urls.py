from django.conf.urls import patterns, url
from tmms import views

urlpatterns = patterns('',
    url(r'^index/show/TestResults', views.pivot_table),
    url(r'^index/show/(\w+)', views.show_main_2),
    url(r'^index/queryeditor/', views.create_query),  
    url(r'^index/ajax/([-#\w]+)', views.show_main),  
    url(r'^index/$', views.index),
    url(r'^index/pivot/', views.pivot_table),    
    )