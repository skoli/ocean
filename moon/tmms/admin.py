from django.contrib import admin

from tmms.models import Testcase, ImpStatus, NwSrv, Feature, Rat
from tmms.models import ImpProgress, Tag, TestcaseTagging
from tmms.models import Measurement, TempVolt, Band
from tmms.models import Customer, Product, Release, Verdict
from tmms.models import TestResults2G, TestResults3G, TestResults4G
from tmms.models import TestCampaignName, TestPlan
from tmms.models import TestParam2G, TestParam3G, TestParam4G, TestParamVamos

class RatAdmin(admin.ModelAdmin):
    list_display = ('id', 'rat')

class NwSrvAdmin(admin.ModelAdmin):
    list_display = ('id', 'rat', 'network') 

class TestcaseAdmin(admin.ModelAdmin):
    list_display = ('id', 'tc_id', 'tc_title', 'feature', 'nw_srv', 'imp_status', 'description', 'reference')
    list_filter = ('nw_srv', 'imp_status')
    search_fields = ('tc_id', 'tc_title',)

class ImpProgressAdmin(admin.ModelAdmin):
    list_display = ('progress_datestamp', 'progress_timestamp', 'imp_status', 'count')

class BandAdmin(admin.ModelAdmin):
    list_display = ('id', 'rat', 'band')

class TempVoltAdmin(admin.ModelAdmin):
    list_display = ('id', 'temp', 'volt')


class TestParam2GAdmin(admin.ModelAdmin):
    list_display = ('id', 'alias', 'tch_power_level', 'tch_time_slot', 'number_of_slots', 'pcl')
    
class TestParam3GAdmin(admin.ModelAdmin):
    list_display = ('id', 'alias', 'beta_c', 'beta_d')
    
class TestParam4GAdmin(admin.ModelAdmin):
    list_display = ('id', 'alias', 'dl_rb', 'ul_rb', 'channel_type', 'rb_pos')
    
class TestParamVamosAdmin(admin.ModelAdmin):
    list_display = ('id', 'tsc_set_active_user', 'tsc_number_active_user', 'scpir')

class TestcaseTaggingAdmin(admin.ModelAdmin):
    list_display = ('id', 'tag', 'testcase')


admin.site.register(Rat, RatAdmin)
admin.site.register(NwSrv, NwSrvAdmin)
admin.site.register(Feature)
admin.site.register(ImpStatus)
admin.site.register(Testcase, TestcaseAdmin)
admin.site.register(ImpProgress, ImpProgressAdmin)

admin.site.register(Band, BandAdmin)
admin.site.register(Measurement)
admin.site.register(TempVolt, TempVoltAdmin)

admin.site.register(Tag)

admin.site.register(Verdict)
admin.site.register(TestCampaignName)


admin.site.register(TestParamVamos, TestParamVamosAdmin)   
admin.site.register(TestParam2G, TestParam2GAdmin)
admin.site.register(TestParam3G, TestParam3GAdmin)
admin.site.register(TestParam4G, TestParam4GAdmin)

#class TestParameterAdmin(admin.ModelAdmin):
#    list_display = ('id', 'rat', 'test_param_2g', 'test_param_3g', 'test_param_4g', 'duration')
#admin.site.register(TestParameter, TestParameterAdmin)


admin.site.register(TestcaseTagging, TestcaseTaggingAdmin)


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('name', 'details')

class ProductAdmin(admin.ModelAdmin):
    list_display = ('customer', 'product')

class ReleaseAdmin(admin.ModelAdmin):
    list_display = ('product', 'hw_rel', 'sw_rel')

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Release, ReleaseAdmin)

#class TestCampaignAdmin(admin.ModelAdmin):
#    list_display = ('title', 'testcase')

class TestPlanAdmin(admin.ModelAdmin):
    list_display = ('release', 'campaign2g', 'campaign3g', 'campaign4g')

#admin.site.register(TestCampaign, TestCampaignAdmin)
admin.site.register(TestPlan, TestPlanAdmin)


class TestResults2GAdmin(admin.ModelAdmin):
    list_display = ('id', 'release', 'testcase', 'test_param','data', 'unit', 'upper_limit', 'lower_limit', 'verdict', 'exec_timestamp')

admin.site.register(TestResults2G, TestResults2GAdmin)

class TestResults3GAdmin(admin.ModelAdmin):
    list_display = ('id', 'release', 'testcase', 'test_param','data', 'unit', 'upper_limit', 'lower_limit', 'verdict', 'exec_timestamp')

admin.site.register(TestResults3G, TestResults3GAdmin)

class TestResults4GAdmin(admin.ModelAdmin):
    list_display = ('id', 'release', 'testcase', 'test_param','data', 'unit', 'upper_limit', 'lower_limit', 'verdict', 'exec_timestamp')

admin.site.register(TestResults4G, TestResults4GAdmin)