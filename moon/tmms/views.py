from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
import json
import tmms
from tmms.models import TestResults2G, Query
from tmms.utils import MyJSONEncoder, GetSlickGridColDef, GetFieldsName
from django.db import models
from django_pandas.io import read_frame 
from django.core.context_processors import request
#from django.contrib.auth.decorators import login_required

def show_main(request, name):
    print name
    template = 'ajax/' + name + ".html"
    print template
    return render(request, template)

def show_main_2(request, name):
    print show_main_2
    name = name.lower()
    models= {k.lower():v for k,v in tmms.models.__dict__.items()}
    if name in models:
        model = models[name]
    else:
        model = models['product'] 
    
    data= []
    columns = GetSlickGridColDef(model)
    qs = model.objects.select_related().all()
        
    raw_data = serializers.serialize('python', qs, use_natural_keys=True)
    
    for d in raw_data:
        p = d['fields']
        p['id'] = d['pk']
        data.append(p)

    return render(request, 'content2.html', {'data':json.dumps(data, cls=MyJSONEncoder), 'columns': columns, 'content_type':'application/json'})

def index(request):
    return render(request, 'index.html')   

def create_query(request):
    data = []
    
    i = 0
    app = models.get_app('tmms')
    for model in models.get_models(app):
        i += 1
        fields = GetFieldsName(model)
        if len(fields):
            for field in fields:
                node ={}
                node['id'] = i
                node['table'] = model.__name__
                node['field'] = field
                data.append(node)
                i += 1
    return render(request, 'queryeditor.html', {'data':json.dumps(data), 'content_type':'application/json'})

def pivot_table(request):
    qs = TestResults2G.objects.all().order_by('-release')#.filter(release__exact=3)
    #print qs
    #df = read_frame(qs, fieldnames=['temp_volt', 'band', 'channel', 'testcase', 'release', 'verdict'])
    #print df
    pt = qs.to_pivot_table(values='verdict',rows=['temp_volt', 'band', 'channel', 'testcase', 'test_param',
                            'measurement' ], 
                           cols=['release'], aggfunc=lambda x: x, fill_value = 'Not Run')
    
    return render(request, 'content5.html', {'data':pt.to_html(classes="table table-striped")})


def main():
    data = create_query()
    print data   
    
if '__main__' == __name__:
    main()