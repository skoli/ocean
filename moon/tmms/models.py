from django.db import models
from models_managers import *
from jsonfield import JSONField
import collections
from django_pandas.managers import DataFrameManager
from django.contrib.auth.models import User


class Customer(models.Model):
    objects = CustomerManager()
    name =  models.CharField(max_length=100)
    details = models.TextField(null=True, blank=True)
    
    def natural_key(self):
        return self.name
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        db_table = 'customer'

class Product(models.Model):
    objects = ProductManager()
    customer = models.ForeignKey(Customer)
    product = models.CharField(max_length=50)

    def natural_key(self):
        return self.product
    
    def __unicode__(self):
        return self.product
    
    class Meta:
        db_table = 'product'

class Release(models.Model):
    objects = ReleaseManager()
    product = models.ForeignKey(Product)
    hw_rel = models.CharField(max_length=100)
    sw_rel = models.CharField(max_length=100)

    def natural_key(self):
        return self.sw_rel
    
    def __unicode__(self):
        return self.sw_rel
    
    class Meta:
        db_table = 'release'

class Verdict(models.Model): 
    objects = VerdictManager()
    verdict = models.CharField(max_length=40)
    
    def natural_key(self):
        return self.verdict
    
    def __unicode__(self):
        return self.verdict
    
    class Meta:
        db_table = 'verdict'
        
class Rat(models.Model):
    rat = models.CharField(max_length=2)
    
    def natural_key(self):
        return self.rat
    
    def __unicode__(self):
        return self.rat
    
    class Meta:
        db_table = 'rat'
       
class NwSrv(models.Model):
    rat = models.ForeignKey(Rat)
    network = models.CharField(max_length=20)
    
    def natural_key(self):
        return self.network
    
    def __unicode__(self):
        return self.network
    
    class Meta:
        db_table = 'nw_srv'

class Band(models.Model):
    objects = BandManager()
    rat = models.ForeignKey(Rat)
    band = models.CharField(max_length=10)
    
    def natural_key(self):
            return self.band
        
    def __unicode__(self):
        return self.band
    
    class Meta:
        db_table = 'band'

class Feature(models.Model):
    feature = models.CharField(max_length=50)

    def natural_key(self):
        return self.feature

    def __unicode__(self):
        return self.feature
    
    class Meta:
        db_table = 'feature'

class TempVolt(models.Model):
    objects = TempVoltManager() 
    temp = models.CharField(max_length=5, default='NT')
    volt = models.CharField(max_length=5, default='NV')
    
    def natural_key(self):
        return "%s-%s" %(self.temp, self.volt)
    def __unicode__(self):
        return "%s-%s" %(self.temp, self.volt)
    
    class Meta:
        db_table = 'temp_volt'

class Measurement(models.Model):
    objects = MeasurementManager()
    measurement = models.CharField(max_length=100)
    
    def natural_key(self):
        return self.measurement
    
    def __unicode__(self):
        return self.measurement
    
    class Meta:
        db_table = 'measurement'

class ImpStatus(models.Model):
    status = models.CharField(max_length=50)
    
    def natural_key(self):
        return self.status
    
    def __unicode__(self):
        return self.status
    
    class Meta:
        db_table = 'imp_status'

class Testcase(models.Model):
    objects = TestcaseManager()
    tc_id = models.CharField(max_length=50)
    tc_title = models.CharField(max_length=250)
    feature = models.ForeignKey(Feature, null=True, blank=True)
    nw_srv = models.ForeignKey(NwSrv)
    reference = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    imp_status = models.ForeignKey(ImpStatus)
    
    def natural_key(self):
        return self.tc_id

    def __unicode__(self):
        return self.tc_id
    
    class Meta:
        db_table = 'testcase'

class ImpProgress(models.Model):
    objects = ImpProgressManager()
    progress_datestamp = models.DateField()
    progress_timestamp = models.DateTimeField()
    imp_status = models.ForeignKey(ImpStatus)
    count = models.IntegerField()
    
    def natural_key(self):
        return self.state
    
    def __unicode__(self):
        return "%s-%s-%s"%(self.progress_timestamp, self.imp_status, self.count)
    
    class Meta:
        db_table = 'imp_progress'

class CommonParam(models.Model):
    alias = models.CharField(max_length=100, default="TBD") # could be useful in pivot table
    nw_srv = models.ForeignKey(NwSrv)
    trans_scheme = models.CharField(max_length=10)
    call_type = models.CharField(max_length=4)
    test_mode = models.CharField(max_length=10, null=True, blank=True)
    dl_coding_scheme = models.CharField(max_length=20)
    ul_coding_scheme = models.CharField(max_length=20)
    fading_setup = models.CharField(max_length=40)
    fading_profile = models.CharField(max_length=40)
    antenna_config = models.CharField(max_length=20)
    duration = models.IntegerField(blank=True, null=True)
    
    class Meta:
        abstract = True 

class TestParamVamos(models.Model):
    objects = TestParamVamosManager()
    alias = models.CharField(max_length=100, default="TBD") # could be useful in pivot table 
    vamos_level = models.CharField(max_length=2)
    active_subchannel = models.IntegerField()
    tsc_set_active_user = models.IntegerField() 
    tsc_number_active_user = models.IntegerField()
    tsc_set_other_user = models.IntegerField()
    tsc_number_other_user = models.IntegerField()
    scpir = models.CharField(max_length=4)
    vamos_profile = models.CharField(max_length=20)
    
    def natural_key(self):
        return "%s-%s-%s-%s" %(self.vamos_level, self.active_subchannel, self.scpir, self.vamos_profile)
    
    def __unicode__(self):
        return "%s-%s-%s-%s" %(self.vamos_level, self.active_subchannel, self.scpir, self.vamos_profile)
    
    class Meta:
        db_table = 'test_param_vamos'

class TestParam2G(CommonParam):
    objects = TestParam2GManager() 
    tch_power_level = models.CharField(max_length=6)
    tch_time_slot = models.IntegerField() #should be 2..7
    number_of_slots = models.IntegerField() # should be 1..4
    amr_channel_rate = models.CharField(max_length=10, null=True, blank=True)
    pcl = models.CharField(max_length=4)
    freq_hopping = models.BooleanField(default=False)
    vamos_setting = models.ForeignKey(TestParamVamos, null=True, blank=True)
    
    def natural_key(self):
        return "%s (%s)" %(self.alias, self.id)
    
    def __unicode__(self):
        return "%s (%s)" %(self.alias, self.id)
    
    class Meta:
        db_table = 'test_param_2g'

class TestParam3G(CommonParam):
    objects = TestParam3GManager()
    cpich_level = models.CharField(max_length=6)
    dpdch_level = models.CharField(max_length=6)
    Ior_Ioc = models.CharField(max_length=6)
    Ior = models.CharField(max_length=6)
    ue_output_power = models.CharField(max_length=6)
    beta_c = models.IntegerField()
    beta_d = models.IntegerField()
    
    def natural_key(self):
        return "%s (%s)" %(self.alias, self.id)
    
    def __unicode__(self):
        return "%s (%s)" %(self.alias, self.id)
    
    class Meta:
        db_table = 'test_param_3g'

class InterferenceParam4G(models.Model):
    objects = InterferenceParam4GManager()
    alias = models.CharField(max_length=100, default="TBD") # could be useful in pivot table
    interf_nw = models.CharField(max_length=6)
    interf_cell_bw = models.CharField(max_length=6)
    interf_modulation = models.CharField(max_length=6)
    interf_freq_offset = models.CharField(max_length=6)
    
    def natural_key(self):
        return "%s (%s)" %(self.alias, self.id)
    
    def __unicode__(self):
        return "%s (%s)" %(self.alias, self.id)
    
    class Meta:
        db_table = 'interference_param_4g'     
    
class TestParam4G(CommonParam):
    objects = TestParam4GManager()
    bandwidth = models.CharField(max_length=6)
    cell_power =  models.CharField(max_length=6)
    ns_value = models.CharField(max_length=5)
    dl_rb = models.IntegerField()
    ul_rb = models.IntegerField()
    channel_type = models.CharField(max_length=10)
    rb_pos = models.IntegerField()
    ue_ouput_power = models.CharField(max_length=6)
    snr = models.CharField(max_length=6)
    interf_param = models.ForeignKey(InterferenceParam4G, null=True, blank=True)
    
    def natural_key(self):
        return "%s (%s)" %(self.alias, self.id)
    
    def __unicode__(self):
        return "%s (%s)" %(self.alias, self.id)
    
    class Meta:
        db_table = 'test_param_4g'

class TestCampaignName(models.Model):
    objects = TestCampaignManager()
    title = models.CharField(max_length=100)
    product = models.ForeignKey(Product, blank=True, null=True)
    
    def natural_key(self):
        return self.title
        
    def __unicode__(self):
        return self.title
    
    class Meta:
        db_table = 'test_campaign_name'

class TestCampaign(models.Model):
    title = models.ForeignKey(TestCampaignName)
    temp_volt = models.ForeignKey(TempVolt)
    rat = models.ForeignKey(Rat)
    band = models.ForeignKey(Band)
    channel = models.IntegerField()
    testcase = models.ForeignKey(Testcase)
        
    class Meta:
        #db_table = 'test_campaign'
        abstract = True

class TestCampaign2G(TestCampaign):
    test_param = models.ForeignKey(TestParam2G)
    
    class Meta:
        db_table = 'test_campaign_2g'

class TestCampaign3G(TestCampaign):
    test_param = models.ForeignKey(TestParam3G)
    
    class Meta:
        db_table = 'test_campaign_3g'

class TestCampaign4G(TestCampaign):
    test_param = models.ForeignKey(TestParam4G)
    
    class Meta:
        db_table = 'test_campaign_4g'
                
class TestPlan(models.Model):
    release = models.ForeignKey(Release)
    campaign2g = models.ForeignKey(TestCampaign2G, blank=True, null=True)
    campaign3g = models.ForeignKey(TestCampaign3G, blank=True, null=True)
    campaign4g = models.ForeignKey(TestCampaign4G, blank=True, null=True)
    
    class Meta:
        db_table = 'test_plan'

class TestResults(models.Model):
    #objects = TestResultsManager()
    objects = DataFrameManager()
    release = models.ForeignKey(Release)
    temp_volt = models.ForeignKey(TempVolt)
    band = models.ForeignKey(Band)
    channel = models.IntegerField()
    testcase = models.ForeignKey(Testcase)
    #test_parameter = models.ForeignKey(TestParameter)
    measurement = models.ForeignKey(Measurement)
    number_of_frames = models.IntegerField(null=True, blank=True)
    data = JSONField(load_kwargs={'object_pairs_hook': collections.OrderedDict}, null=True, blank=True)
    unit = models.CharField(max_length=10, null=True, blank=True)
    upper_limit = models.CharField(max_length=10, null=True, blank=True)
    lower_limit = models.CharField(max_length=10, null=True, blank=True)
    verdict = models.ForeignKey(Verdict)
    exec_timestamp = models.DateTimeField()
    results_file_path = models.URLField()

    class Meta:
        #db_table = 'test_result'
        abstract = True

class TestResults2G(TestResults):
    test_param = models.ForeignKey(TestParam2G)
    
    class Meta:
        db_table = 'test_results_2g'
 
class TestResults3G(TestResults):
    test_param = models.ForeignKey(TestParam3G)
    
    class Meta:
        db_table = 'test_results_3g'

class TestResults4G(TestResults):
    test_param = models.ForeignKey(TestParam4G)
    
    class Meta:
        db_table = 'test_results_4g'
 
    
class Tag(models.Model):
    tag = models.CharField(max_length=40)

    def __unicode__(self):
        return self.tag
    
    class Meta:
        db_table = 'tag'

class TestcaseTagging(models.Model):
    tag = models.ForeignKey(Tag)
    testcase = models.ForeignKey(Testcase)

    def __unicode__(self):
        return "%s-%s" %(self.tag, self.testcase)
    
    class Meta:
        db_table = 'testcase_tagging'
    
class Query(models.Model):
    user = models.ForeignKey(User)
    query_name = models.CharField(max_length=200)
    field_list = models.TextField()
    filter = models.TextField()
    
    def __unicode__(self):
        return self.query_name
    
    class Meta: 
        db_table = 'query'
        
    